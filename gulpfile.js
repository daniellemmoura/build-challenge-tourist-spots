const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));

function styles () {
  return gulp
  .src('src/styles/main.scss')
  .pipe(sass({outputStyle: "compressed"}).on('error', sass.logError))
  .pipe(gulp.dest('dist'));
}

function sentinel () {
  gulp.watch('src/styles/*.scss', { ignoreInitial: false} ,styles);
}

exports.sentinel = sentinel;