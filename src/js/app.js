document.addEventListener('DOMContentLoaded', function() {

    let form = document.querySelector('.tourist-spots-form');
    let file = document.querySelector('#image');

    let  titleInput = document.querySelector("#lista-imagens");
    let descriptionInput = document.querySelector("#lista-imagens");

    function logFile (event) {
        debugger;
        let str = event.target.result;
        let img = document.createElement('img');
        img.src = str;
        document.querySelector("#lista-imagens").append(img);
        //console.log(str);

        let title = document.createElement('h2');
        title.push = titleInput;
    }
    
    function handleSubmit (event) {

        // Stop the form from reloading the page
        debugger;
        event.preventDefault();
    
        // If there's no file, do nothing
        if (!file.value.length) return;
    
        // Create a new FileReader() object
        let reader = new FileReader();
    
        // Setup the callback event to run when the file is read
        reader.onload = logFile;
    
        // Read the file
        reader.readAsDataURL(file.files[0]);
    
                
        renderListItems();
        resetInputs();
      
    }

    
    function resetInputs() {
        imageInput.value = "";
        titleInput.value = "";
        descriptionInput.value = "";
    }

    form.addEventListener('submit', handleSubmit);

});
